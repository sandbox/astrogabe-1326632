<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 */
?>
<!-- <div id="wrapper" class="clearfix"> -->
<div id="page" class="clearfix container">

<div id="header" class="clearfix"><!--start header--> 
    <div id="header-top" class="clearfix">
	<div id="logo"><!--start logo-->
	<?php if ($logo): ?>
	<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
	<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
	</a>
	<?php endif; ?>
	<div id="site-slogan"><?php if ($site_slogan): ?><?php print $site_slogan; ?><?php endif; ?></div><!--site slogan-->
	</div><!--end logo-->
	 <?php if ($page['search_box']): ?><!-- / start search box region -->
    	<div class="search-box">
      	<?php print render($page['search_box']); ?>
    	</div> <!-- / end search box region -->
	<?php endif; ?>
	<ul id="header-social">
	<li><a href="http://www.twitter.com/<?php echo theme_get_setting('twitter_username'); ?>" target="_blank" rel="me"><img src="<?php global $base_url; echo $base_url.'/'.$directory.'/'; ?>/images/twitter.png" alt="twitter"/></a></li>
	<li><a href="http://www.facebook.com/<?php echo theme_get_setting('facebook_username'); ?>" target="_blank" rel="me"><img src="<?php global $base_url; echo $base_url.'/'.$directory.'/'; ?>/images/facebook.png" alt="facebook"/></a></li>
	<li><a href="<?php print $front_page; ?>rss.xml"><img src="<?php global $base_url; echo $base_url.'/'.$directory.'/'; ?>/images/rss.png" alt="RSS"/></a></li>
	</ul><!--end header-social-->
	</div><!--end header-top-->
    <?php print render($page['header']); ?>
	<div id="main-menu">
	<div id="main-menu-links">
    <?php 
	$main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu')); 
	print drupal_render($main_menu_tree);
	?>
	</div><!-- end main-menu-links -->
	</div><!-- end main-menu -->
</div> <!-- /#header -->

<?php if ($page['menu_bar']): ?>
      <?php print render($page['menu_bar']); ?>
<?php endif; ?>

<!-- <div id="content-body"> -->
<div id="columns"><div class="columns-inner clearfix">
    <div id="content-column"><div class="content-inner">
	
  <?php if ($breadcrumb): ?>
		<div id="breadcrumb">
		<h2 class="element-invisible"><?php print $breadcrumbheading; ?></h2>
		<?php print $breadcrumb; ?>
		</div>
  <?php endif; ?>
  <section id="main" role="main" class="clear">
    <?php print $messages; ?>
    <a id="main-content"></a>
    <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper clearfix"><?php print render($tabs); ?></div><?php endif; ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
    <div id="content"><?php print render($page['content']); ?></div>
  </section> <!-- /#main -->
  
  </div></div>
  
  <?php if ($page['sidebar_first']): ?>
    <!-- <aside id="sidebar-first" role="complementary" class="sidebar clearfix"> -->
      <?php print render($page['sidebar_first']); ?>
    <!-- </aside> -->  <!-- /#sidebar-first -->
  <?php endif; ?>

  <?php if ($page['sidebar_second']): ?>
    <!-- <aside id="sidebar-second" role="complementary" class="sidebar clearfix"> -->
      <?php print render($page['sidebar_second']); ?>
    <!-- </aside> --> <!-- /#sidebar-second -->
  <?php endif; ?>

</div></div>
<!-- </div> --> <!-- end content-body -->

<div class="clear"></div>
<div id="footer" class="clearfix">
 <div class="first-second-wrapper">
 <?php if ($page['footer_first']): ?><!-- / start first footer block -->
    <div class="first-footer">
      <?php print render($page['footer_first']); ?>
    </div> <!-- / end first footer -->
  <?php endif; ?>
 <?php if ($page['footer_second']): ?><!-- / start second footer block -->
    <div class="second-footer">
      <?php print render($page['footer_second']); ?>
    </div> <!-- / end second footer -->
  <?php endif; ?>
 </div> <!-- /#first-two-wrapper -->
 <div class="third-fourth-wrapper"> 
 <?php if ($page['footer_third']): ?><!-- / start third footer block -->
    <div class="second-footer">
      <?php print render($page['footer_third']); ?>
    </div> <!-- / end third footer -->
  <?php endif; ?>
 <?php if ($page['footer_fourth']): ?><!-- / start fourth footer block -->
    <div class="second-footer">
      <?php print render($page['footer_fourth']); ?>
    </div> <!-- / end fourth footer -->
  <?php endif; ?>
 </div> <!-- /#third-fourth-wrapper -->
<div class="clear"></div>
<?php print render($page['footer']) ?>
<div class="clear"></div>
<div id="copyright">Copyright &copy; <?php echo date("Y"); ?>, <?php print $site_name; ?></div>
</div> <!-- /#footer -->
</div> <!-- /#page -->