<?php
/**
 * @file
 * theme-settings.php
 */
/**
Insert option to enter facebook and twitter username
 */
function responsivetouch_form_system_theme_settings_alter(&$form, &$form_state) {

  if (isset($form_id)) {
    return;
  }
  
  drupal_add_css(drupal_get_path('theme', 'adaptivetheme') . '/css/at.settings.form.css', array('group' => CSS_THEME));

// Social Media settings
$form['at']['socialmedia'] = array(
  '#type' => 'fieldset',
  '#title' => t('Social Media'),
  '#weight' => 25,
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
$form['at']['socialmedia']['twitter_username'] = array(
  '#type' => 'textfield',
  '#title' => t('Twitter Username'),
  '#default_value' => theme_get_setting('twitter_username'),
  '#description'   => t("Enter your Twitter username."),
);
$form['at']['socialmedia']['facebook_username'] = array(
  '#type' => 'textfield',
  '#title' => t('Facebook Username'),
  '#default_value' => theme_get_setting('facebook_username'),
  '#description'   => t("Enter your Facebook username."),
);
  
  
// Typography settings
$form['at']['typography']['theme_font_config'] = array(
  '#type' => 'fieldset',
  '#title' => t('Typography'),
  '#weight' => 27,
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
);
  
$form['at']['typography']['theme_font_config']['theme_font_config_font']['theme_font'] = array(
  '#type'          => 'select',
  '#title'         => t('Select a new font family'),
  '#default_value' => theme_get_setting('theme_font'),
  '#options'       => array(
  'none' => t('Default'),
  'font-family-sans-serif-sm' => t('Sans serif - small (Helvetica Neue, Arial, Helvetica)'),
  'font-family-sans-serif-lg' => t('Sans serif - large (Verdana, Geneva, Arial, Helvetica)'),
  'font-family-serif-sm' => t('Serif - small (Garamond, Perpetua, Times New Roman)'),
  'font-family-serif-lg' => t('Serif - large (Baskerville, Georgia, Palatino, Book Antiqua)'),
  'font-family-myriad' => t('Myriad (Myriad Pro, Myriad, Trebuchet MS, Arial, Helvetica)'),
  'font-family-lucida' => t('Lucida (Lucida Sans, Lucida Grande, Verdana, Geneva)'),
  ),
);
  
$form['at']['typography']['theme_font_config']['theme_font_config_size']['theme_font_size'] = array(
  '#type'          => 'select',
  '#title'         => t('Change the base font size'),
  '#description'   => t('Adjusts all text in proportion to your base font size. Be aware that changing font size may break the page layout: experiment to see the right size.'),
  '#default_value' => theme_get_setting('theme_font_size'),
  '#options'       => array(
  'font-size-10' => t('10px'),
  'font-size-11' => t('11px'),
  'font-size-12' => t('12px'),
  'font-size-13' => t('13px - Default'),
  'font-size-14' => t('14px'),
  'font-size-15' => t('15px'),
  'font-size-16' => t('16px'),
  'font-size-17' => t('17px'),
  'font-size-18' => t('18px'),
  'font-size-19' => t('19px'),
  'font-size-20' => t('20px'),
  'font-size-21' => t('21px'),
  'font-size-22' => t('22px'),
  'font-size-23' => t('23px'),
  'font-size-24' => t('24px'),
  ),
);
  
  // Style schemes
if (theme_get_setting('style_enable_schemes') == 'on') {
  $form['at']['style'] = array(
  '#type' => 'fieldset',
  '#title' => t('Style'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
  '#weight' => 26,
  '#description'   => t('Use these settings to modify the style of your theme, such as the color scheme.'),
);
$form['at']['style']['style_schemes'] = array(
  '#type' => 'select',
  '#title' => t('Styles'),
  '#default_value' => theme_get_setting('style_schemes'),
  '#options' => array(
  'style-default.css' => t('Default'),
  'trueblue.css' => t('True Blue Style'),
  'serenegreen.css' => t('Serene Green Style'),
  'downtownbrown.css' => t('Downtown Brown Style'),
  ),
);
$form['at']['style']['style_enable_schemes'] = array(
  '#type' => 'hidden',
  '#value' => theme_get_setting('style_enable_schemes'),
);
}
}