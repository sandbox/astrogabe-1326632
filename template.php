<?php
/**
 * @file
 * template.php
 */
/**
 * Load media queries css for responsive web design.
 */
function responsivetouch_preprocess_html(&$variables) {
  $media_queries_css = array(
  'responsivetouch.responsive.style.css',
  'responsivetouch.responsive.gpanels.css'
  );
  load_subtheme_media_queries($media_queries_css, 'responsivetouch');
}


/**
 * Change theme styles set in theme settings.
 */
function get_responsivetouch_styles() {
  $scheme = theme_get_setting('style_schemes');
  if (!$scheme) {
    $scheme = 'style-default.css';
  }
  if (isset($_COOKIE["responsivetouchstyles"])) {
    $scheme = $_COOKIE["responsivetouchstyles"];
  }
  return $scheme;
}
if (theme_get_setting('style_enable_schemes') == 'on') {
  $style = get_responsivetouch_styles();
  if ($style != 'none') {
    drupal_add_css(path_to_theme() . '/css/schemes/' . $style, array('group' => CSS_THEME, 'preprocess' => TRUE));
  }
}


/**
 * Process html to add typography theme settings to <body> tag classes.
 */
function responsivetouch_process_html(&$vars) {
  $classes = explode(' ', $vars['classes']);
  $classes[] = (theme_get_setting('theme_font') != 'none') ? theme_get_setting('theme_font') : '';
  $classes[] = theme_get_setting('theme_font_size');
  $classes = array_filter($classes);
  $vars['classes'] = trim(implode(' ', $classes));
}


/**
 * Implements hook_html_head_alter().
 * This will overwrite the default meta character type tag with HTML5 version.
 */
function responsivetouch_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
  'charset' => 'utf-8'
  );
}

/**
 * Insert themed breadcrumb page navigation at top of the node content.
 */
/*
function responsivetouch_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Use CSS to hide titile .element-invisible.
    $output = '<h2 class="element-invisible">' . t('Breadcrumb: You are here') . '</h2>';
    // comment below line to hide current page to breadcrumb
	$breadcrumb[] = drupal_get_title();
    $output .= '<nav class="breadcrumb">' . implode(' » ', $breadcrumb) . '</nav>';
    return $output;
  }
}
 */

/**
 * Override or insert variables into the page template.
 */
function responsivetouch_preprocess_page(&$vars) {
  if (isset($vars['main_menu'])) {
    $vars['main_menu'] = theme('links__system_main_menu', array(
    'links' => $vars['main_menu'],
    'attributes' => array(
    'class' => array('links', 'main-menu', 'clearfix'),
    ),
    'heading' => array(
    'text' => t('Main menu'),
    'level' => 'h2',
    'class' => array('element-invisible'),
    )
    ));
  }
  else {
    $vars['main_menu'] = FALSE;
  }
  if (isset($vars['secondary_menu'])) {
  $vars['secondary_menu'] = theme('links__system_secondary_menu', array(
  'links' => $vars['secondary_menu'],
  'attributes' => array(
  'class' => array('links', 'secondary-menu', 'clearfix'),
  ),
  'heading' => array(
  'text' => t('Secondary menu'),
  'level' => 'h2',
  'class' => array('element-invisible'),
  )
));
}
  else {
    $vars['secondary_menu'] = FALSE;
  }
  
  // AT adds <a> anchor tag to site name; this strips html tag from site name */
  if (!empty($vars['site_name'])) {
  $vars['site_name'] = strip_tags($vars['site_name']);
  }
  $vars['breadcrumbheading'] = 'Breadcrumb';
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function responsivetouch_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
  $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
  $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
  $variables['primary']['#suffix'] = '</ul>';
  $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
  $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
  $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
  $variables['secondary']['#suffix'] = '</ul>';
  $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Override or insert variables into the node template.
 */
function responsivetouch_preprocess_node(&$variables) {
  $variables['submitted'] = t('By !username on !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}


/**
 * Remove Drupal default menu bullets.
 */
function responsivetouch_menu_link(array $vars) {
  $element = $vars['element'];
  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  if (theme_get_setting('extra_menu_classes')) {
    $element['#attributes']['class'][] = 'menu-item-' . $element['#original_link']['mlid'];
  }
  if (theme_get_setting('menu_item_span_elements')) {
    $element['#title'] = '<span>' . $element['#title'] . '</span>';
    $element['#localized_options']['html'] = TRUE;
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li>' . $output . $sub_menu . "</li>";
  }